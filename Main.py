import requests
import urllib
from bs4 import BeautifulSoup


def main():
    s = requests.session()
    s.headers['User-Agent']='Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.131 Safari/537.36'
    videoGames = input("entrer un jeux vidéo\n")
    query = urllib.parse.quote(videoGames)
    url = "https://www.metacritic.com/search/all/"+query+"/results"
    page = s.get(url)
    soup = BeautifulSoup(page.content, "html.parser")
    note = soup.find("span", class_="metascore_w medium game positive")
#    print(soup.prettify())
    print(note.text)


if __name__ == "__main__":
    main()
